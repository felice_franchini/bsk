package tdd.training.bsk;

import java.util.ArrayList;

public class Game {
	
	private ArrayList<Frame> frames = new ArrayList<Frame>();
	private int firstBonusThrow;
	private int secondBonusThrow;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		
		frames.add(frame);
		
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		
		if(index < 0 || index > 9) throw new BowlingException("Frame Inesistente");
		
		return frames.get(index);
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		
		int score = 0;
		
		for (int i = 0; i < frames.size(); i++) {

			Frame frame = frames.get(i);

			if (i < 9) {

				Frame nextFrame = frames.get(i + 1);

				if (frame.isStrike()) {

					if (nextFrame.isStrike()) {

						if (i < 8) frame.setBonus(nextFrame.getFirstThrow() + frames.get(i + 2).getFirstThrow());
						else frame.setBonus(nextFrame.getFirstThrow() + firstBonusThrow);

					} else frame.setBonus(nextFrame.getFirstThrow() + nextFrame.getSecondThrow());

				} else if (frame.isSpare()) frame.setBonus(nextFrame.getFirstThrow());

			} else {

				if (frame.isSpare()) frame.setBonus(firstBonusThrow);
				if (frame.isStrike()) frame.setBonus(firstBonusThrow + secondBonusThrow);

			}

			score += frame.getScore();

		}
		
		return score;
        
	}

}
