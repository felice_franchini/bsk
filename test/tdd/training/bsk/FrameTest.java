package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;

public class FrameTest {

	@Test
	public void testInputFirstThrow() throws Exception {
		
		int firstThrow = 2;
		int secondThrow = 4;
		
		Frame frame = new Frame(firstThrow, secondThrow);
		
		assertEquals(firstThrow, frame.getFirstThrow());		
	}
	
	@Test
	public void testInputSecondThrow() throws Exception {
		
		int firstThrow = 2;
		int secondThrow = 4;
		
		Frame frame = new Frame(firstThrow, secondThrow);
		
		assertEquals(secondThrow, frame.getSecondThrow());		
	}

	@Test
	public void testOrdinaryFrameScore() throws Exception {
		
		int firstThrow = 2;
		int secondThrow = 6;
		
		Frame frame = new Frame(firstThrow, secondThrow);
		
		assertEquals(firstThrow + secondThrow, frame.getScore());		
	}
	
	@Test
	public void testInputBonus() throws Exception {
		
		int firstThrow = 4;
		int secondThrow = 6;
		int bonus = 7;
		
		Frame frame = new Frame(firstThrow, secondThrow);
		frame.setBonus(bonus);
		
		assertEquals(bonus, frame.getBonus());		
	}
	
	@Test
	public void testFrameNotSpare() throws Exception {
		
		int firstThrow = 3;
		int secondThrow = 6;
		
		Frame frame = new Frame(firstThrow, secondThrow);
		
		assertFalse(frame.isSpare());		
	}
	
	@Test
	public void testFrameSpare() throws Exception {
		
		int firstThrow = 4;
		int secondThrow = 6;
		
		Frame frame = new Frame(firstThrow, secondThrow);
		
		assertTrue(frame.isSpare());		
	}
	
	@Test
	public void testSpareFrameScore() throws Exception {
		
		int firstThrow = 4;
		int secondThrow = 6;
		int bonus = 7;
		
		Frame frame = new Frame(firstThrow, secondThrow);
		frame.setBonus(bonus);
		
		assertEquals(firstThrow + secondThrow + bonus, frame.getScore());		
	}
	
	@Test
	public void testFrameNotStrike() throws Exception {
		
		int firstThrow = 4;
		int secondThrow = 6;
		
		Frame frame = new Frame(firstThrow, secondThrow);
		
		assertFalse(frame.isStrike());		
	}
	
	@Test
	public void testFrameStrike() throws Exception {
		
		int firstThrow = 10;
		int secondThrow = 6;
		
		Frame frame = new Frame(firstThrow, secondThrow);
		
		assertTrue(frame.isStrike());		
	}
	
	@Test
	public void testStrikeFrameScore() throws Exception {
		
		int firstThrow = 10;
		int secondThrow = 0;
		int bonus = 9;
		
		Frame frame = new Frame(firstThrow, secondThrow);
		frame.setBonus(bonus);
		
		assertEquals(firstThrow + secondThrow + bonus, frame.getScore());		
	}
}
