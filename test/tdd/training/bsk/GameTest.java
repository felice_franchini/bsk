package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {

	@Test
	public void testGetTheIThFrame() throws Exception{
		Game game = new Game();
		Frame frame6 = new Frame(3, 3);
		
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(frame6);
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(frame6, game.getFrameAt(6));
	}
	
	@Test (expected = BowlingException.class)
	public void testGetOutOfBoundFrame() throws Exception{
		Game game = new Game();
		
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		game.getFrameAt(10);
	}
	
	@Test
	public void testCalculateScore() throws Exception{
		Game game = new Game();
		
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(81, game.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithSpareFrames() throws Exception{
		Game game = new Game();
		
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 3)); //spare
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 7)); //spare
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(93, game.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithStrikeFrames() throws Exception{
		Game game = new Game();
		
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(10, 0)); //strike
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(94, game.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithSpareAndStrikeFrames() throws Exception{
		Game game = new Game();
		
		game.addFrame(new Frame(10, 0)); //strike
		game.addFrame(new Frame(4, 6)); //spare
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(103, game.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithMultipleStrikesFrames() throws Exception{
		Game game = new Game();
		
		game.addFrame(new Frame(10, 0)); //strike
		game.addFrame(new Frame(10, 0)); //strike
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(112, game.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithMultipleSparesFrames() throws Exception{
		Game game = new Game();
		
		game.addFrame(new Frame(8, 2)); //spare
		game.addFrame(new Frame(5, 5)); //spare
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(98, game.calculateScore());
	}
	
	@Test
	public void testInputBonusThrow() throws Exception {
		
		int firstBonusThrow = 7;
		Game game = new Game();
		
		game.setFirstBonusThrow(firstBonusThrow);
		
		assertEquals(firstBonusThrow, game.getFirstBonusThrow());
	}
	
	@Test
	public void testCalculateScoreLastFrameSpare() throws Exception{
		Game game = new Game();
		
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 8));
		game.setFirstBonusThrow(7);
		
		assertEquals(90, game.calculateScore());
	}
	
	@Test
	public void testInputSecondBonusThrow() throws Exception {
		
		int secondBonusThrow = 7;
		Game game = new Game();
		
		game.setSecondBonusThrow(secondBonusThrow);
		
		assertEquals(secondBonusThrow, game.getSecondBonusThrow());
	}

	@Test
	public void testCalculateScoreLastFrameStrike() throws Exception{
		Game game = new Game();
		
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(10, 0));
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		assertEquals(92, game.calculateScore());
	}
	
	@Test
	public void testCalculateScorePerfectGame() throws Exception{
		Game game = new Game();
		
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		
		assertEquals(300, game.calculateScore());
	}

}
